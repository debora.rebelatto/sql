CREATE TABLE if not exists products (
	product_id integer PRIMARY KEY not null,
	name char(50) not null,
	amount integer not null,
	price integer not null,
	id_providers integer not null,
	id_categories integer not null
);


CREATE TABLE if not exists categories (
	category_id integer PRIMARY KEY not null,
	name char(50) not null
	/*constraint pk_category primary key (category_id)*/
);

CREATE TABLE if not exists providers (
	provider_id integer PRIMARY KEY not null,
	name char(50) not null,
	street char(50) not null,
	city char(50) not null,
	state char(2) not null
	/*constraint pk_provider primary key(provider_id)*/
);


/*CREATE TABLE vendors(
	vendor_code integer PRIMARY KEY,
	vendor_name character(35),
	vendor_city character(15),
	vendor_country character(20)
);*/


CREATE TABLE orders(
	ord_no integer PRIMARY KEY,
	ord_date date,
	item_code integer REFERENCES items(item_code),
	item_grade character(1),
	ord_qty numeric,ord_amount numeric
);