-- sudo -i -u postgres
-- psql
\l -- List database
\c hotel; -- Connect to database
\d reservation; -- Show table

-- Create database
CREATE DATABASE hotel;

DROP TABLE if exists reservation cascade; -- Drop if already exists
DROP TABLE if exists room cascade;
DROP TABLE if exists guests cascade;

-- Create table
CREATE TABLE reservation (
	reservation_id integer PRIMARY KEY not null,
	guest_id integer not null,
	room_id integer not null,
	reservation DATE not null,
	check_in DATE not null,
	check_out DATE not null,
	adults integer not null,
	children integer not null
);

CREATE TABLE room (
	room_id integer PRIMARY KEY not null,
	room_type_id char(50) not null,
	room_floor char(50) not null,
	room_number char(50) not null,
	room_description char(100) not null,
	room_status_id integer
);

CREATE TABLE guests(
	guest_id integer PRIMARY KEY not null,
	first_name char(50) not null,
	last_name char(50) not null,
	guest_address char(50) not null,
	guest_city char(50) not null,
	guest_state char(50) not null,
	guest_cep char(50) not null,
	guest_country char(50) not null,
	guest_residence_number char(50),
	guest_cellular_number char(50) not null,
	guest_email char(50) not null,
	guest_gender char(50) not null
);
