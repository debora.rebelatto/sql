-- Script to build export products database 
-- first check if the tables already exist. If so, drop them (be careful when to running this script!!!)
-- do not forget to connect to a properly database
drop table if exists country cascade;
create table country (
  acrc varchar(5) not null,
  namec varchar(30) not null,
  npres varchar(30) not null,
  confl varchar(1),
  constraint pk_country primary key (acrc)
);
--
drop table if exists product cascade;
create table product (
  codpr integer not null,
  namep varchar(30) not null,
  unit  varchar(5) not null,
  prodt varchar(1) not null,
  pvalid date,
  cpat varchar(5),
  constraint pk_product primary key (codpr),
  constraint fk_product_country foreign key (cpat) references country(acrc)
);
--
drop table if exists destprod;
create table destprod (
  acrp varchar(5) not null,
  codpr  integer not null,
  constraint pk_destprod primary key (acrp, codpr),
  constraint fk_destprod_country foreign key (acrp) references country(acrc),
  constraint fk_destprod_product foreign key (codpr) references product(codpr)
);
--
drop table if exists prodpurp cascade;
create table prodpurp (
   codpp integer not null,
   dscr varchar(20) not null,
   constraint pk_prodpurp primary key (codpp)
);
--
drop table if exists prodfeat;
create table prodfeat (
   codpr integer not null,
   codpp integer not null,
   constraint pk_prodfeat primary key (codpr,codpp),
   constraint fk_prodfeat_product foreign key (codpr) references product(codpr),
   constraint fk_prodfeat_prodpurp foreign key (codpp) references prodpurp(codpp)
);
--
drop table if exists prodcomp;
create table prodcomp (
 codpr integer not null,
 codpr_c integer not null,
 qty numeric(6,2) not null,
 constraint pk_prodcomp primary key (codpr,codpr_c),
 constraint fk_prodcomp_product1 foreign key (codpr) references product(codpr),
 constraint fk_prodcomp_product2 foreign key (codpr_c) references product(codpr)
);
-- that's all
--- Exercices
/*
  Given the following reports, insert the rows in their tables
  ** Product list
  code name	               unit type valid date patent
  ---- ------------------  ---- ---- ---------- ------
  1    Rackstar Pallets    Un   O
  2    1/2 Steel Strapping Mt   0               FRA
  3    Adalimumab          Cp   1    05/30/2020 USA
  4    Pregabalin          Cp   1    12/31/2019 ENG
  5    Rituximab           Ml   1    01/31/2020 SPA
  6    Kinor-35H camera    Un                   RUS
  
  ** Country list
  Acronym Name                      President            Conflict?
  ------- ------------------------- -------------------- ---------
  POR     Portugal                  Anibal Cavaco Silva  N
  USA     United States of America  Donald Trump         N
  RUS     Russia                    Vladimir Putin       N
  FRA     France                    Emmanuel Macron      N
  SPA     Spain                     Pedro Sanchez        N
  ARG     Argentine                 Mauricio Macri       N
  CAN     Canada                    Justin Trudeau       N
  IRQ     Iraq                      Barham Salih         S
  
  ** Product purpose
  Code Description
  ---- -----------
  1    Industry
  2    Military
  3    Home
  4    Health
  
  ** List of products and their purposes
  Name	              Purposes
  -----------------   -----------
  Rackstar Pallets    Industry Home
  Rituximab           Health
  Kinor-35H camera    Home
  1/2 Steel Strapping Military Home Industry
  
  ** List of importin products by country
  Country    Product
  Portugal   Rackstar Pallets
  Portugal   Pregabalin
  Argentine  1/2 Steel Strapping
  Argentine  Kinor-35H camera
  France     Adalimumab
  France     Rackstar Pallets
  IRQ        Rackstar Pallets
  IRQ        Rituximab
  IRQ        Adalimumab

  ** insert a composite product called New Age (create a free code)
     it is composed of 3 units of Rituximab e 2 units of Adalimumab
  ** insert a composite product called Industry Facility (create a free code)
     it is composed of 1 units of Rackstar Pallets e 4 units of 1/2 Steel Strapping
  ** Update Portugal's president to Marcelo R. de Souza
  ** Delete Rituximab from the list of Iraq import products
*/
     
