CREATE DATABASE library;

CREATE TABLE if not exists books (
	bookid integer not null,
	title char(50) not null,
	author char(50) not null,
	year date not null,
	publisher char(50) not null,
	edition char(50) not null,
	call char(50) not null,
	constraint pk_bookid primary key (bookid)
);

CREATE TABLE users (
	userid integer not null,
	name char(50) not null
	birthdate date not null,
	
);